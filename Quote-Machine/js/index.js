var quoteList = [
  "Hello, World! --Anon Programmer",
  "Up and to the right! --Thomas was Alone",
  "I never quit, unless I'm dead or completely incapaciated. --Elon Musk",
  "Imagination encompasses the world"
];

function getRandomQuote(){
  currentQuote = document.getElementById("quoteText").innerHTML;
  nextQuote = quoteList[Math.floor(Math.random() * quoteList.length)];
  
  if(currentQuote === nextQuote){
    return getRandomQuote();
  }else{
    document.getElementById("quoteText").innerHTML = nextQuote;
  }
}

function tweetQuote(){
  currentQuote = document.getElementById("quoteText").innerHTML;
  tweetURL = 'https://twitter.com/intent/tweet?text=' + '"' + currentQuote + '"';
  
  window.open(tweetURL);
}