var fahrenheit = true;

function setupForecast(){
  setLocationText();
  updateWeather();
}

function setLocationText(){
  $.getJSON("https://ipinfo.io", function(response) {
    document.getElementById("locationText").innerHTML = response.city + ", " + response.region;
  }, "jsonp");
}

function updateWeather(){
  if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition(function(position){
      darkSkyAPICall(position.coords.latitude, position.coords.longitude);
    });
  }else{
    console.log("Can't access location");
  }
}

function darkSkyAPICall(lat, long){
  var url = "https://api.darksky.net/forecast/02b96e345c8d0352254124efc93d2ed3/" + lat + "," + long + "/?units=auto&exclude=minutely,daily";
  
  $.getJSON(url + "&callback=?").done(updatePageWeather);
}

function updatePageWeather(weatherData){
  var curTemp = weatherData.currently.temperature;
  updateBackground(curTemp);
  updateWeatherIcon(weatherData.currently.icon);
  
  if(fahrenheit){
    document.getElementById("temperatueText").innerHTML = Math.floor(curTemp) + "°F";
  }else{
    document.getElementById("temperatueText").innerHTML = Math.floor((curTemp - 32) * (5.0/9.0)) + "°C";
  }
  
  document.getElementById("hourlySummary").innerHTML = weatherData.hourly.summary;
  document.getElementById("windSpeed").innerHTML = "Wind Speed: " + weatherData.currently.windSpeed + " km/hr";
}

function updateWeatherIcon(icon){
  var icons = new Skycons({"color" : "black"});
  var theIcon;
  
  switch(icon){
    case "clear-day":
      theIcon = Skycons.CLEAR_DAY;
      break;
    case "clear-night":
      theIcon = Skycons.CLEAR_NIGHT;
      break;
    case "partly-cloudy-day":
      theIcon = Skycons.PARTLY_CLOUDY_DAY;
      break;
    case "partly-cloud-night":
      theIcon = Skycons.PARTLY_CLOUDY_NIGHT;
      break;
    case "cloudy":
      theIcon = Skycons.CLOUDY;
      break;
    case "rain":
      theIcon = Skycons.RAIN;
      break;
    case "sleet":
      theIcon = Skycons.SLEET;
      break;
    case "snow":
      theIcon = Skycons.SNOW;
      break;
    case "wind":
      theIcon = Skycons.WIND;
      break;
    case "fog":
      theIcon = Skycons.FOG;
      break;
    default:
      theIcon = Skycons.CLEAR_DAY;
      break;
  }
  
  icons.set("weatherIcon", theIcon);
  icons.play();
}

function updateBackground(temp){
  var bgColor;
  var titleBarColor;
  
  if(temp >= 90){
    bgColor = "#efd723";
    titleBarColor = "#d6c020";
  }else if(temp <= 90 && temp >= 80){
    bgColor = "#ffa42d";
    titleBarColor = "#e29126";
  }else if(temp < 80 && temp >= 50){
    bgColor = "#1da3d8";
    titleBarColor = "#1992c1";
  }else{
    return;
  }
  
  document.body.style.backgroundColor = bgColor;
  document.getElementById("headingJumbotron").style.backgroundColor = titleBarColor;
}

function changeTempMeasurment(){
  if(fahrenheit){
    fahrenheit = false;
    document.getElementById("changeTempMeasurementButton").innerHTML = "Change to Fahrenheit.";
  }else{
    fahrenheit = true;
    document.getElementById("changeTempMeasurementButton").innerHTML = "Change to Celsius.";
  }
  
  updateWeather();
}