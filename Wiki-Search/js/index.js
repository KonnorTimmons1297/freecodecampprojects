var queryResults;

function randomWikipage(){
  window.open("https://en.wikipedia.org/wiki/Special:Random", "_blank");
}

$(document).keypress(function(event){
    if(event.keyCode == 13){
     $('#searchButton').click();
    }
});

function submitWikipediaQuery(){
  var queryText = document.getElementById('wikiSearch').value;
  var url;

  if(queryText){
    url = createWikiURL(queryText);
    $.getJSON(url + "&callback=?").done(updateResults);
  }
}

function createWikiURL(queryText){
  apiURLBase = "https://en.wikipedia.org/w/api.php?action=opensearch&format=json&search=";

  queryText = queryText.split(' ');

  for(var i = 0; i < queryText.length; i++){
    apiURLBase += queryText[i];

    if(i !== queryText.length - 1){
      apiURLBase += "+";
    }
  }

  return apiURLBase;
}

function updateResults(wikiData){
  var previousResults = false;

  if(document.getElementById("searchResult1Row")){
    previousResults = true;
  }

  for(var i = 0; i < 10; i++){
    var currentResult = {
      "number" : i,
      "name" : wikiData[1][i],
      "summary" : wikiData[2][i],
      "link" : wikiData[3][i]
    };

    if(previousResults){
      updateNewResults(currentResult);
    }else{
      createNewResult(currentResult);
    }
  }
}

function createNewResult(newResult){
  var newResultRow = createSearchResultRow(newResult.number);
  document.body.appendChild(newResultRow);

  var newResultCol = createSearchResultCol(newResult.number);
  newResultRow.appendChild(newResultCol);

  var newResultHeader = createSearchResultHeader(newResult);
  newResultCol.appendChild(newResultHeader);

  var newResultSummary = createSearchResultSummary(newResult.summary);
  newResultCol.appendChild(newResultSummary);
}
function createSearchResultSummary(summary){
  var summaryOfResultPar = document.createElement('h5');
  var summaryOfResult = document.createTextNode(summary);

  summaryOfResultPar.appendChild(summaryOfResult);

  return summaryOfResultPar;
}
function createSearchResultHeader(newResult){
  var nameOfResultAnchor = document.createElement('a');
  var nameOfResultHeader = document.createElement('h3');
  var nameOfResultTextNode = document.createTextNode(newResult.name);

  nameOfResultAnchor.href = newResult.link;
  nameOfResultHeader.appendChild(nameOfResultTextNode);
  nameOfResultAnchor.appendChild(nameOfResultHeader);
  nameOfResultAnchor.style.textAlign = "left";

  return nameOfResultAnchor;
}
function createSearchResultCol(resultNumber){
  var resultDivCol = document.createElement('div');

  resultDivCol.id = "searchResult" + resultNumber + "Col";
  resultDivCol.className = "col-lg-12 content-col round-corners";
  resultDivCol.style.backgroundColor = "white";


  return resultDivCol;
}
function createSearchResultRow(resultNumber){
  var resultDivRow = document.createElement('div');

  resultDivRow.id = "searchResult" + resultNumber + "Row";
  resultDivRow.className = "row content-row";
  resultDivRow.style.paddingTop = "10px";
  resultDivRow.style.paddingRight = "15%";
  resultDivRow.style.marginLeft = "15%";

  return resultDivRow;
}

function updateNewResults(newResult){
  updateSearchResultHeader(newResult);
  updateSearchResultSummary(newResult);
}
function updateSearchResultHeader(result) {
  var searchResultColumnChildren_Anchor = document.getElementById("searchResult" + result.number + "Col").childNodes[0];
  var searchResultColumnChildren_Title = searchResultColumnChildren_Anchor.childNodes[0];

  searchResultColumnChildren_Anchor.href = result.link;
  searchResultColumnChildren_Title.textContent = result.name;
}
function updateSearchResultSummary(result) {
  var searchResultColumnChildren_Summary = document.getElementById("searchResult" + result.number + "Col").childNodes[1];

  searchResultColumnChildren_Summary.textContent = result.summary;
}
